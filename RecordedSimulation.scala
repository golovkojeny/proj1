
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.core.structure.{ChainBuilder, ScenarioBuilder}
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class RecordedSimulation extends Simulation {


  

  val httpProtocol = http
    .baseUrl("https://app.testing.digitalbpm.io")
    .wsBaseUrl("wss://app.testing.digitalbpm.io").disableWarmUp
    .inferHtmlResources()
    
val headers_login = Map(
      "Accept" -> "application/json",
      "Accept-Encoding" -> "gzip, deflate, br",
      "Accept-Language" -> "uk,ru;q=0.9,en;q=0.8,en-US;q=0.7,ru-RU;q=0.6",
      "Connection" -> "keep-alive",
      "Content-Length" -> "83",
      "Content-Type" -> "application/json",
      "Host" -> "app.testing.digitalbpm.io",
      "Origin" -> "https://app.testing.digitalbpm.io",
      "Referer" -> "https://app.testing.digitalbpm.io/app/log-in",
      "sec-ch-ua" -> """Chromium";v="107", "Google Chrome";v="107", "Not;A=Brand";v="24""",
      "sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows",
      "Sec-Fetch-Dest" -> "empty",
      "Sec-Fetch-Mode" -> "ors",
      "Sec-Fetch-Site" -> "same-origin",
      "User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"


)
  val headers_0 = Map(
  		"Upgrade-Insecure-Requests" -> "1",
      "Origin" -> "https://app.testing.digitalbpm.io",
      "User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
  		"sec-ch-ua" -> """Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows"
    
  )

  val headers_16 = Map(
  		"Accept" -> "application/json, text/plain, */*",
  		"Accept-Encoding" -> "gzip, deflate, br",
  		"Accept-Language" -> "uk,ru;q=0.9,en;q=0.8,en-US;q=0.7,ru-RU;q=0.6",
  		"Origin" -> "https://app.testing.digitalbpm.io",
  		"Sec-Fetch-Dest" -> "empty",
  		"Sec-Fetch-Mode" -> "cors",
  		"Sec-Fetch-Site" -> "same-origin",
  		"User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
  		"sec-ch-ua" -> """Chromium";v="106", "Google Chrome";v="106", "Not;A=Brand";v="99""",
  		"sec-ch-ua-mobile" -> "?0",
  		"sec-ch-ua-platform" -> "Windows"
  )
  
  val WS = Map(

     
      "Host" -> "app.testing.digitalbpm.io",
      "Connection" -> "Upgrade",
      "Pragma" -> "no-cache",
      "Cache-Control" -> "no-cache",
      "User-Agent" -> "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36",
      "Upgrade" -> "websocket",
      "Origin" -> "https://app.testing.digitalbpm.io",
      "Sec-WebSocket-Version" -> "13",
      "Accept-Encoding" -> "gzip, deflate, br",
  		"Accept-Language" -> "uk,ru;q=0.9,en;q=0.8,en-US;q=0.7,ru-RU;q=0.6",
      "Cookie" -> "sid=#{sid}",
      "Sec-WebSocket-Key" -> "Kda6YOO65lO9bbwUopebiA==",
      "Sec-WebSocket-Extensions" -> "permessage-deflate; client_max_window_bits"
      
      
      
      
      )
  
  
  val uri1 = "https://www.google-analytics.com"
  
  val uri3 = "https://digitalbpm.s3-eu-central-1.amazonaws.com/avatars/2a430143-69c1-4152-9ec9-2867d8879d54"

  val scn = scenario("RecordedSimulation")

object Status{
 val scn = scenario("RecordedSimulation")
    .exec(
      http("get_status")
        .post("/api/v1/session/get_status")
        .headers(headers_16)
       
    .check(jsonPath("$.response.sid").findAll.transform( _.mkString(",") ).saveAs("sidfirst"))
		.check(status is 200)
    )
}


  object Login{

	 val jsonFileFeeder = jsonFile("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/bpm/login.json").circular
	 val scn = scenario("RecordedSimulation")
	 .feed(jsonFileFeeder)
    .exec(http("login")
        .post("/api/v1/account/log_in")
       
          .body(StringBody("""{
            "email": "${email}",
        	"password": "${password}",
			"long_session": ${long_session}}"""
      )).asJson
      .check(status is 200))
}
    
   
  object Status2{
 val scn = scenario("RecordedSimulation")
    .exec(
      http("get_status2")
        .post("/api/v1/session/get_status")
        .headers(headers_16)
       
    .check(jsonPath("$.response.sid").findAll.transform( _.mkString(",") ).saveAs("sid"))
		.check(status is 200)
    )
.exec(
			ws("Connect WS").connect("/ws")
		
			.headers(WS)
			.onConnected(

      /* DASBOARD */ 

				exec(
					ws("statistic.get_transactions_and_tasks")
		        .sendText("""{"action":"statistic.get_transactions_and_tasks", "test_key": "1"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.statistic.get_transactions_and_tasks")
							.matching(jsonPath("$.action").is("statistic.get_transactions_and_tasks"))
              .check(jsonPath("$.response").saveAs("file.statistic.get_transactions_and_tasks"))
							
						)

        )

        			exec(
					ws("statistic.get_info_by_services")
		        .sendText("""{"action":"statistic.get_info_by_services", "test_key": "2"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.statistic.get_info_by_services")
							.matching(jsonPath("$.action").is("statistic.get_info_by_services"))
              .check(jsonPath("$.response").saveAs("file.statistic.get_info_by_services"))
							
						)

        )
            			exec(
					ws("statistic.get_info_by_scenarios")
		        .sendText("""{"action":"statistic.get_info_by_scenarios", "test_key": "3"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.statistic.get_info_by_scenarios")
							.matching(jsonPath("$.action").is("statistic.get_info_by_scenarios"))
              .check(jsonPath("$.response").saveAs("file.statistic.get_info_by_scenarios"))
							
						)

        )
       			exec(
					ws("subscription.get_billing_info")
		        .sendText("""{"action":"subscription.get_billing_info", "test_key": "4"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.subscription.get_billing_info")
							.matching(jsonPath("$.action").is("subscription.get_billing_info"))
              .check(jsonPath("$.response").saveAs("file.subscription.get_billing_info"))
							
						)

        )
          			exec(
					ws("company.get_members")
		        .sendText("""{"action":"company.get_members", "test_key": "5"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.company.get_members")
							.matching(jsonPath("$.action").is("company.get_members"))
              .check(jsonPath("$.response").saveAs("file.company.get_members"))
							
						)

        )
         			exec(
					ws("company.get_available")
		        .sendText("""{"action":"company.get_available", "test_key": "6"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.company.get_available")
							.matching(jsonPath("$.action").is("company.get_available"))
              .check(jsonPath("$.response").saveAs("file.company.get_available"))
							
						)

        )

        /* BILLING */ 

         			exec(
					ws("payment_method.get")
		        .sendText("""{"action":"payment_method.get", "test_key": "7"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.payment_method.get")
							.matching(jsonPath("$.action").is("payment_method.get"))
              .check(jsonPath("$.response").saveAs("file.payment_method.get"))
							
						)

        )

              exec(
					ws("payment_method.get_default")
		        .sendText("""{"action":"payment_method.get_default", "test_key": "8"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.payment_method.get_default")
							.matching(jsonPath("$.action").is("payment_method.get_default"))
              .check(jsonPath("$.response").saveAs("file.payment_method.get_default"))
							
						)

        )

         			exec(
					ws("payment.get_all")
		        .sendText("""{"action":"payment.get_all", "test_key": "9", "limit": "10", "offset": "0"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.payment.get_all")
							.matching(jsonPath("$.action").is("payment.get_all"))
              .check(jsonPath("$.response").saveAs("file.payment.get_all"))
							
						)

        )

        /* TEAM */ 

              exec(
					ws("company.get_members")
		        .sendText("""{"action":"company.get_members", "test_key": "10"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.company.get_members")
							.matching(jsonPath("$.action").is("company.get_members"))
              .check(jsonPath("$.response").saveAs("file.company.get_members"))
							
						)

        )

              exec(
					ws("company.get_roles")
		        .sendText("""{"action":"company.get_roles", "test_key": "11"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.company.get_roles")
							.matching(jsonPath("$.action").is("company.get_roles"))
              .check(jsonPath("$.response").saveAs("file.company.get_roles"))
							
						)

        )

        /* AUTOMATION */ 


                     
             exec(
					ws("folder.get_main")
		        .sendText("""{"action":"folder.get_main", "test_key": "12"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.folder.get_main")
							.matching(jsonPath("$.action").is("folder.get_main"))
              .check(jsonPath("$.response").saveAs("file.folder.get_main"))
              .check(jsonPath("$.response.folder_id").saveAs("folder_id"))
              
							
						)

        )

             exec(
					ws("folder.get_all")
		        .sendText("""{"action":"folder.get_all", "folder_id": #{folder_id}}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.folder.get_all")
							.matching(jsonPath("$.action").is("folder.get_all"))
              .check(jsonPath("$.response").saveAs("file.folder.get_all"))
							
						)

        )

        /* SCENARIO */ 

             exec(
					ws("scenario.create")
		        .sendText("""{"action":"scenario.create", "folder_id": #{folder_id}, "is_open_after_creation": true, "name": "1at"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.scenario.create")
							.matching(jsonPath("$.action").is("scenario.create"))
              .check(jsonPath("$.response").saveAs("file.scenario.create"))
              .check(jsonPath("$.response.scenario_id").saveAs("scenario_id"))
							.check(jsonPath("$.response.version_id").saveAs("version_id"))
						)

        )

             exec(
					ws("scenario.add_user")
		        .sendText("""{"action":"scenario.add_user", "scenario_id": "#{scenario_id}"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.scenario.add_user")
							.matching(jsonPath("$.action").is("scenario.add_user"))
              .check(jsonPath("$.response").saveAs("file.scenario.add_user"))
							
						)

        )

              exec(
					ws("scenario.get_all_users")
		        .sendText("""{"action":"scenario.get_all_users", "scenario_id": "#{scenario_id}"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.scenario.get_all_users")
							.matching(jsonPath("$.action").is("scenario.get_all_users"))
              .check(jsonPath("$.response").saveAs("file.scenario.get_all_users"))
							
						)

        )

              exec(
					ws("scenario.get")
		        .sendText("""{"action":"scenario.get", "scenario_id": "#{scenario_id}", "version_id": "#{version_id}"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.scenario.get")
							.matching(jsonPath("$.action").is("scenario.get"))
              .check(jsonPath("$.response").saveAs("file.scenario.get"))
							
						)

        )

        /* BLOCKS */ 



              exec(
					ws("block.update_init")
		        .sendText("""{"action":"block.update_init", "init": {"name": "Block t1", "x_coordinate": 100, "y_coordinate": 100}, "scenario_id": "#{scenario_id}", "version_id": "#{version_id}"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.update_init")
							.matching(jsonPath("$.action").is("block.update_init"))
              .check(jsonPath("$.response").saveAs("file.block.update_init"))
							
						)

        )

              exec(
					ws("service.get_webhook_services")
		        .sendText("""{"action":"service.get_webhook_services"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.service.get_webhook_services")
							.matching(jsonPath("$.action").is("service.get_webhook_services"))
              .check(jsonPath("$.response").saveAs("file.service.get_webhook_services"))
							
						)

        )


              exec(
					ws("service.get_all_webhook_methods")
		        .sendText("""{"action":"service.get_all_webhook_methods", "service_id": "3e2f7f1f-7df8-4cf7-8b80-97eb25b83e72"}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.service.get_all_webhook_methods")
							.matching(jsonPath("$.action").is("service.get_all_webhook_methods"))
              .check(jsonPath("$.response").saveAs("file.service.get_all_webhook_methods"))
							
						)

        )


                      exec(
					ws("block.add_first")
		        .sendText("""{"action":"block.add_first", "scenario_id": "#{scenario_id}", "version_id": "#{version_id}", "x_coordinate": 400, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add_first")
							.matching(jsonPath("$.action").is("block.add_first"))
              .check(jsonPath("$.response").saveAs("file.block.add_first"))
              .check(jsonPath("$.response.block_id").saveAs("block_id"))
							
						)

        )

                              exec(
					ws("block.add")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 700, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_sec"))
							
						)

        )

                          exec(
					ws("block.add3")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_sec}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 1000, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_thi"))
							
						)

        )

                        exec(
					ws("block.add4")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_thi}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 1300, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_for"))
							
						)

        )

                    exec(
					ws("block.add5")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_for}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 1600, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_why"))
							
						)

        )

                            exec(
					ws("block.add6")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_why}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 1900, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_ziz"))
							
						)

        )

                                    exec(
					ws("block.add7")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_ziz}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 2200, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_zoo"))
							
						)

        )

                                            exec(
					ws("block.add8")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_zoo}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 2500, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_kia"))
							
						)

        )

                                                    exec(
					ws("block.add9")
		        .sendText("""{"action":"block.add", "is_branching_block": false, "parent_block_id": "#{block_id_kia}", "scenario_id": "#{scenario_id}", "source_position": "b", "version_id": "#{version_id}", "x_coordinate": 2800, "y_coordinate": 100}""")
						.await(10.seconds)
						(ws.checkTextMessage("response.block.add")
							.matching(jsonPath("$.action").is("block.add"))
              .check(jsonPath("$.response").saveAs("file.block.add"))
              .check(jsonPath("$.response.block_id").saveAs("block_id_kia2"))
							
						)

        )



      )

)
  
.exec(ws("Close WS").close)
  }


  object Print{
    val scn = scenario("RecordedSimulation")
	.exec( session => {

			  println(session("sidfirst").as[String])
        println(session("sid").as[String])
        println(session("folder_id").as[String])
        println(session("scenario_id").as[String])
        println(session("version_id").as[String])

  /* DASBOARD */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("1 - statistic.get_transactions_and_tasks"+session("file.statistic.get_transactions_and_tasks").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("2 - statistic.get_info_by_services"+session("file.statistic.get_info_by_services").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("3 - statistic.get_info_by_scenarios"+session("file.statistic.get_info_by_scenarios").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("4 - subscription.get_billing_info"+session("file.subscription.get_billing_info").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("5 - company.get_members"+session("file.company.get_members").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("6 - company.get_available"+session("file.company.get_available").as[String]+"\n"+"\n")

  /* BILLING */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("7 - payment_method.get"+session("file.payment_method.get").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("8 - payment_method.get_default"+session("file.payment_method.get_default").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("9 - payment.get_all"+session("file.payment.get_all").as[String]+"\n"+"\n")

  /* TEAM */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("10 - company.get_members"+session("file.company.get_members").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("11 - company.get_roles"+session("file.company.get_roles").as[String]+"\n"+"\n")

  /* AUTOMATION */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("12 - folder.get_main"+session("file.folder.get_main").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("13 - folder.get_all"+session("file.folder.get_all").as[String]+"\n"+"\n")

  /* SCENARIO */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("14 - scenario.create"+session("file.scenario.create").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("15 - scenario.add_user"+session("file.scenario.add_user").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("16 - scenario.get_all_users"+session("file.scenario.get_all_users").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("17 - scenario.get"+session("file.scenario.get").as[String]+"\n"+"\n")

  /* BLOCKS */ 

  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("18 - block.update_init"+session("file.block.update_init").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("19 - service.get_webhook_services"+session("file.service.get_webhook_services").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("20 - block.update_init"+session("file.block.update_init").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("21 - service.get_all_webhook_methods"+session("file.service.get_all_webhook_methods").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("22 - block.add_first"+session("file.block.add_first").as[String]+"\n"+"\n")
  scala.tools.nsc.io.File("C:/gatling-charts-highcharts-bundle-3.8.4/user-files/resources/data/responses.csv").appendAll("23 - block.add"+session("file.block.add").as[String]+"\n"+"\n")
  
session}
  )
  
  }
   


    
  


   val users = scenario("Users").exec(Status.scn, Login.scn, Status2.scn, Print.scn)
    setUp(users.inject(
            nothingFor(1),
            atOnceUsers(1),
            constantUsersPerSec(1) during (1)
         ))
        .protocols(httpProtocol)
}

