package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.Duration;

public class Tester2 {
    private WebDriver chromeDriver;

    private static final String baseUrl = "https://do.nmu.org.ua/";

    @BeforeClass(alwaysRun = true)
    public void setUp() {
        //Run driver
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        //set fullscreen
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.addArguments("--remote-allow-origins=*");
        //setup wait for loading elements
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions(){
        //open main page
        chromeDriver.get(baseUrl);
    }

    @Test
    public void testClickOnButton(){
        //The test is successful. Clicking the button - Catalog
        WebElement clickable = chromeDriver.findElement(By.id("frontpagesettings"));
        clickable.click();
    }
    @Test
    public void testEnterDataFieldAndCheckAndSearch(){
        //The test is successful.
        WebElement searchField = chromeDriver.findElement(By.name("q"));
        //verification
        Assert.assertNotNull(searchField);
        //String inputValue
        String inputValue = "Нейронні мережі";
        searchField.sendKeys(inputValue);
        //different params of searchField
        System.out.println( String.format("Name attribute: %s", searchField.getAttribute("name")) +
                String.format("\nID attribute: %s", searchField.getAttribute("id")) +
                String.format("\nType attribute: %s", searchField.getAttribute("type")) +
                String.format("\nValue attribute: %s", searchField.getAttribute("value")) +
                String.format("\nPosition: (%d;%d)", searchField.getLocation().x, searchField.getLocation().y) +
                String.format("\nSize: %dx%d", searchField.getSize().height, searchField.getSize().width));
        //verification text
        Assert.assertEquals(searchField.getAttribute("value"),inputValue);
        //click enter
        searchField.sendKeys(Keys.ENTER);
    }
    @Test
    public void testClickOnSignIn(){
        //find element by xpath
        WebElement signInButton = chromeDriver.findElement(By.xpath("/html/body/div[1]/nav/div/div[2]/div/span/a"));
        //verification
        Assert.assertNotNull(signInButton);
        signInButton.click();
    }
    @Test
    public void testCheckAnyCondition(){

        WebElement searchField = chromeDriver.findElement(By.name("q"));
        Assert.assertNotNull(searchField);
        String inputValue = "Нейронні мережі";

        searchField.sendKeys(inputValue);
        Assert.assertEquals(searchField.getAttribute("value"), inputValue);

        String search = searchField.getAttribute("value");

        if(search.length() == 0)
            System.out.println("\nНе було зроблено запиту!");
        else
            System.out.println("\nЗапит виконався успішно!");
    }
}
