package org.example.Lab2;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.hc.core5.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class Indiv {
    private static final String baseUrl = "https://petstore.swagger.io/v2";

    private static final String PET = "/pet",
    PET_petid = PET + "/{petId}";

    private String petId;
    private String petName;

    private String tagName = "EugeneHolovko";
    private String category = "122-20";

    private String statusPet = "available";

    private static final String STORE = "/store/order",
            STORE_ORDER_GET = STORE + "/{orderId}";

    private String orderId;
    @BeforeClass
    public void setUp(){
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }
    @Test
    public void verifyAddPet() {
        String bodys = " {\n" +
                "  \"category\": {\n" +
                "    \"id\": 0,\n" +
                "    \"name\": \"122-20ск-1\"\n" +
                "  },\n" +
                "  \"name\": \"" + Faker.instance().dog().name() + "\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"" + Faker.instance().dog().memePhrase() + "\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 0,\n" +
                "      \"name\": \"" + tagName + "\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        Response response = given()
                .contentType(ContentType.JSON).body(bodys)
                .post(PET);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        petId = response.jsonPath().get("id").toString();
        petName = response.jsonPath().get("name").toString();

        System.out.println("Домашня тварина. ID " + petId + " Name Dog " + petName + " Category "
                + response.jsonPath().get("category.name").toString()
                + " Tag " + response.jsonPath().get("tags.name").toString()
                + " Status " + response.jsonPath().get("status").toString());
    }
    @Test(dependsOnMethods = "verifyAddPet")
    public void verifyGetPet(){
        Response response = given().pathParams("petId", petId)
                .get(PET_petid);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        petName = response.jsonPath().get("name").toString();

        System.out.println("Перегляд інформації. ID " + petId + " Name Dog " + petName + " Category "
                + response.jsonPath().get("category.name").toString()
                + " Tag " + response.jsonPath().get("tags.name").toString()
                + " Status " + response.jsonPath().get("status").toString());
    }
    @Test(dependsOnMethods = "verifyAddPet", priority = 2 )
    public void verifyAddOrder(){
        Map<String, ?> body = Map.of(
                "petId", petId,
                "quantity", Integer.valueOf("3"),
                "shipDate", "2023-03-11T20:34:22.821Z",
                "status", "placed",
                "complete", Boolean.valueOf("true")
        );

        Response response = given()
                .body(body)
                .post(STORE);
        response.then()
                .statusCode(HttpStatus.SC_OK);

        orderId = response.jsonPath().get("id").toString();
        System.out.println("Створення замовлення. ID " + orderId + " ID тварини " + response.jsonPath().get("petId").toString() + " Кількість "
                + response.jsonPath().get("quantity").toString()
                + " Status " + response.jsonPath().get("status").toString());

    }
}
