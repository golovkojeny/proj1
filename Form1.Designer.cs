﻿namespace ValleyOfDeliciousness
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pnlMainMenu = new System.Windows.Forms.Panel();
            this.tab11 = new System.Windows.Forms.Button();
            this.tab10 = new System.Windows.Forms.Button();
            this.tab9 = new System.Windows.Forms.Button();
            this.tab8 = new System.Windows.Forms.Button();
            this.tab7 = new System.Windows.Forms.Button();
            this.tab6 = new System.Windows.Forms.Button();
            this.tab5 = new System.Windows.Forms.Button();
            this.tab4 = new System.Windows.Forms.Button();
            this.tab3 = new System.Windows.Forms.Button();
            this.tab2 = new System.Windows.Forms.Button();
            this.tab1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.MenuDetect = new System.Windows.Forms.Timer(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tab12 = new System.Windows.Forms.Button();
            this.pnlMainMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlMainMenu
            // 
            this.pnlMainMenu.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.pnlMainMenu.Controls.Add(this.tab12);
            this.pnlMainMenu.Controls.Add(this.tab11);
            this.pnlMainMenu.Controls.Add(this.tab10);
            this.pnlMainMenu.Controls.Add(this.tab9);
            this.pnlMainMenu.Controls.Add(this.tab8);
            this.pnlMainMenu.Controls.Add(this.tab7);
            this.pnlMainMenu.Controls.Add(this.tab6);
            this.pnlMainMenu.Controls.Add(this.tab5);
            this.pnlMainMenu.Controls.Add(this.tab4);
            this.pnlMainMenu.Controls.Add(this.tab3);
            this.pnlMainMenu.Controls.Add(this.tab2);
            this.pnlMainMenu.Controls.Add(this.tab1);
            this.pnlMainMenu.Controls.Add(this.panel1);
            this.pnlMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMainMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMainMenu.Name = "pnlMainMenu";
            this.pnlMainMenu.Size = new System.Drawing.Size(60, 725);
            this.pnlMainMenu.TabIndex = 0;
            // 
            // tab11
            // 
            this.tab11.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab11.FlatAppearance.BorderSize = 0;
            this.tab11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab11.Image = ((System.Drawing.Image)(resources.GetObject("tab11.Image")));
            this.tab11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab11.Location = new System.Drawing.Point(0, 571);
            this.tab11.Name = "tab11";
            this.tab11.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab11.Size = new System.Drawing.Size(60, 45);
            this.tab11.TabIndex = 11;
            this.tab11.Tag = "10";
            this.tab11.Text = "          Звіти";
            this.tab11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab11.UseVisualStyleBackColor = true;
            // 
            // tab10
            // 
            this.tab10.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab10.FlatAppearance.BorderSize = 0;
            this.tab10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab10.Image = ((System.Drawing.Image)(resources.GetObject("tab10.Image")));
            this.tab10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab10.Location = new System.Drawing.Point(0, 526);
            this.tab10.Name = "tab10";
            this.tab10.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab10.Size = new System.Drawing.Size(60, 45);
            this.tab10.TabIndex = 10;
            this.tab10.Tag = "9";
            this.tab10.Text = "          Фінанси";
            this.tab10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab10.UseVisualStyleBackColor = true;
            // 
            // tab9
            // 
            this.tab9.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab9.FlatAppearance.BorderSize = 0;
            this.tab9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab9.Image = ((System.Drawing.Image)(resources.GetObject("tab9.Image")));
            this.tab9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab9.Location = new System.Drawing.Point(0, 481);
            this.tab9.Name = "tab9";
            this.tab9.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab9.Size = new System.Drawing.Size(60, 45);
            this.tab9.TabIndex = 9;
            this.tab9.Tag = "8";
            this.tab9.Text = "          Розведення тварин";
            this.tab9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab9.UseVisualStyleBackColor = true;
            // 
            // tab8
            // 
            this.tab8.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab8.FlatAppearance.BorderSize = 0;
            this.tab8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab8.Image = ((System.Drawing.Image)(resources.GetObject("tab8.Image")));
            this.tab8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab8.Location = new System.Drawing.Point(0, 436);
            this.tab8.Name = "tab8";
            this.tab8.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab8.Size = new System.Drawing.Size(60, 45);
            this.tab8.TabIndex = 8;
            this.tab8.Tag = "7";
            this.tab8.Text = "          Продажі";
            this.tab8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab8.UseVisualStyleBackColor = true;
            // 
            // tab7
            // 
            this.tab7.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab7.FlatAppearance.BorderSize = 0;
            this.tab7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab7.Image = ((System.Drawing.Image)(resources.GetObject("tab7.Image")));
            this.tab7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab7.Location = new System.Drawing.Point(0, 391);
            this.tab7.Name = "tab7";
            this.tab7.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab7.Size = new System.Drawing.Size(60, 45);
            this.tab7.TabIndex = 7;
            this.tab7.Tag = "6";
            this.tab7.Text = "          Попереднє замовлення";
            this.tab7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab7.UseVisualStyleBackColor = true;
            // 
            // tab6
            // 
            this.tab6.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab6.FlatAppearance.BorderSize = 0;
            this.tab6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab6.Image = ((System.Drawing.Image)(resources.GetObject("tab6.Image")));
            this.tab6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab6.Location = new System.Drawing.Point(0, 346);
            this.tab6.Name = "tab6";
            this.tab6.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab6.Size = new System.Drawing.Size(60, 45);
            this.tab6.TabIndex = 6;
            this.tab6.Tag = "5";
            this.tab6.Text = "          Товар";
            this.tab6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab6.UseVisualStyleBackColor = true;
            // 
            // tab5
            // 
            this.tab5.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab5.FlatAppearance.BorderSize = 0;
            this.tab5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab5.Image = ((System.Drawing.Image)(resources.GetObject("tab5.Image")));
            this.tab5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab5.Location = new System.Drawing.Point(0, 301);
            this.tab5.Name = "tab5";
            this.tab5.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab5.Size = new System.Drawing.Size(60, 45);
            this.tab5.TabIndex = 5;
            this.tab5.Tag = "4";
            this.tab5.Text = "          Співробітники";
            this.tab5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab5.UseVisualStyleBackColor = true;
            // 
            // tab4
            // 
            this.tab4.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab4.FlatAppearance.BorderSize = 0;
            this.tab4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab4.Image = ((System.Drawing.Image)(resources.GetObject("tab4.Image")));
            this.tab4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab4.Location = new System.Drawing.Point(0, 256);
            this.tab4.Name = "tab4";
            this.tab4.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab4.Size = new System.Drawing.Size(60, 45);
            this.tab4.TabIndex = 4;
            this.tab4.Tag = "3";
            this.tab4.Text = "          Клієнти";
            this.tab4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab4.UseVisualStyleBackColor = true;
            // 
            // tab3
            // 
            this.tab3.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab3.FlatAppearance.BorderSize = 0;
            this.tab3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab3.Image = ((System.Drawing.Image)(resources.GetObject("tab3.Image")));
            this.tab3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab3.Location = new System.Drawing.Point(0, 211);
            this.tab3.Name = "tab3";
            this.tab3.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab3.Size = new System.Drawing.Size(60, 45);
            this.tab3.TabIndex = 3;
            this.tab3.Tag = "2";
            this.tab3.Text = "          Партії";
            this.tab3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab3.UseVisualStyleBackColor = true;
            // 
            // tab2
            // 
            this.tab2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab2.FlatAppearance.BorderSize = 0;
            this.tab2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab2.Image = ((System.Drawing.Image)(resources.GetObject("tab2.Image")));
            this.tab2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab2.Location = new System.Drawing.Point(0, 166);
            this.tab2.Name = "tab2";
            this.tab2.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab2.Size = new System.Drawing.Size(60, 45);
            this.tab2.TabIndex = 2;
            this.tab2.Tag = "1";
            this.tab2.Text = "          Довідники";
            this.tab2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab2.UseVisualStyleBackColor = true;
            // 
            // tab1
            // 
            this.tab1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab1.FlatAppearance.BorderSize = 0;
            this.tab1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab1.Image = ((System.Drawing.Image)(resources.GetObject("tab1.Image")));
            this.tab1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab1.Location = new System.Drawing.Point(0, 121);
            this.tab1.Name = "tab1";
            this.tab1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab1.Size = new System.Drawing.Size(60, 45);
            this.tab1.TabIndex = 1;
            this.tab1.Tag = "0";
            this.tab1.Text = "          Дашборд";
            this.tab1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(60, 121);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // MenuDetect
            // 
            this.MenuDetect.Enabled = true;
            this.MenuDetect.Tick += new System.EventHandler(this.MenuDetect_Tick);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tabControl1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(60, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(268, 725);
            this.panel2.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, -26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(265, 765);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(257, 736);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(257, 736);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tab12
            // 
            this.tab12.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab12.FlatAppearance.BorderSize = 0;
            this.tab12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tab12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tab12.Image = ((System.Drawing.Image)(resources.GetObject("tab12.Image")));
            this.tab12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab12.Location = new System.Drawing.Point(0, 616);
            this.tab12.Name = "tab12";
            this.tab12.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.tab12.Size = new System.Drawing.Size(60, 45);
            this.tab12.TabIndex = 12;
            this.tab12.Tag = "10";
            this.tab12.Text = "          Доставка";
            this.tab12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tab12.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(1347, 725);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnlMainMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1363, 764);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Долина смакоти";
            this.pnlMainMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMainMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button tab10;
        private System.Windows.Forms.Button tab9;
        private System.Windows.Forms.Button tab8;
        private System.Windows.Forms.Button tab7;
        private System.Windows.Forms.Button tab6;
        private System.Windows.Forms.Button tab5;
        private System.Windows.Forms.Button tab4;
        private System.Windows.Forms.Button tab3;
        private System.Windows.Forms.Button tab2;
        private System.Windows.Forms.Button tab1;
        private System.Windows.Forms.Button tab11;
        private System.Windows.Forms.Timer MenuDetect;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button tab12;
    }
}

